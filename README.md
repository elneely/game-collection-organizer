# Game collection organizer

This project will (eventually) allow you to pull a collection of games off of BoardGameGeek and add comments, custom attributes (number of players, length of game, theme, whatever), and filter games based on these attributes.  If you are looking for a quick 2-person game, this should help you narrow down the options in your collection!

## Roadmap

I'm learning pretty much all of the necessary information to do this project, so this will be done very much in stages as I get more comfortable with BeautifulSoup, Flask, etc.

1. [x] Pull data from public BGG list that I have specified and strip the information to print out the game titles.
2. [ ] Instead of just stripping out the HTML, keep it and render the table (because why strip the information just to add it later?)
3. [ ] Store that information in a database that is associated with a user.
4. [ ] Make a user login page to associate with this
     - This requires being able to create an account and password, store that, authenticate that, change it, etc.
5. [ ] Let the user specify the BGG username to generate the collection and gave the collection a name.
6. [ ] Expand the scraped data to include the column with the owned/preordered/etc. column
      - [ ] They need to be able to modify that data
7. [ ] Add certain predetermined (i.e., by me) data columns:
     - Player count
     - Length to play
     - Comments
         - [ ] Figure out how to handle comments and displaying them
8. [ ] Add a search/filter ability for columns
9. [ ] Add the ability for users to create their own categories/columns
10. [ ] Add the ability for users to search/filter their own categories/columns
11. [ ] Add the ability to exclude games from all their searches

## Known issues that aren't on the roadmap yet

1. [ ] How to update the collection from BGG
     - Don't let them update it so often they get their IP address banned, please.
     - We can't just overwrite what we already have because they would lose all of their custom content.
     - Adding games is probably easy because it looks like every game has a unique identifier, so if it doesn't exist in the table already we can just create a new entry. 
     - Deleting is going to be the hard part
     - Do we want to let them keep the information but have the game no longer in their collection?  (i.e., does deleting remove a row from the table or just, say, add a flag to it)
2. [ ] It would be great if we could collapse expansions so that we do not have to wade through 87 separate entries for 7th Continent, say, just because you bought all of the things their Kickstarter offered.
3. [ ] Should there be a way to add things other than through BGG or should we just keep it this way?  (Beware of feature creep.)
4. [ ] I'm putting in the framework to allow people to have multiple collections, but I need to put in a way for them to select which collection they want to look at.

## Contributing

Honestly, since this is a learning project, I'm not hugely looking for contributors at the moment.  I want to figure out how to do this myself!  But perhaps at a future date.  (I will take suggestions, though.)

## Authors and acknowledgments

This project is thusfar written entirely Erica Neely.  However, it is only possible because BoardGameGeek has such a wonderful website (and keeps so much data public.) 

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

This repository should contain a copy of the GNU General Public License. If not, see <https://www.gnu.org/licenses/>.
